
-- SUMMARY --

Adminer (formerly phpMinAdmin) is a full-featured
MySQL management tool written in PHP.
Conversely to phpMyAdmin, it consist of a single file
ready to deploy to the target server.
This plugin include this tool in Drupal for a fast
management of your database.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* Download Adminer from http://www.adminer.org/latest.php
* Put adminer-x.x.x.php into MODULE_PATH/adminer/
* Download required plugins plugin, frames, version-noverify
  from http://www.adminer.org/en/plugins/
* Put downloaded plugins into MODULE_PATH/adminer/plugins/
* Download styles from http://www.adminer.org/#extras
* Put downloaded styles into MODULE_PATH/adminer/styles/
