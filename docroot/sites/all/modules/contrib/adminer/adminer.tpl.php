<?php
/**
 * @file
 * Template for Adminer page.
 */
?>
<iframe id="adminer" name="adminer" src="<?php print $src; ?>" width="100%" height="100%" frameborder="0" scrolling="yes" style="background-color: #FFFFFF; min-height: 600px;">
  <p><?php print $text; ?></p>
</iframe>
