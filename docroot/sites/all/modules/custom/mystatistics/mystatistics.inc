<?php
/**
 * Makes new record in mystatistics table of database
 * @param type $nid - node id to record
 * @param type $uid - user id to record
 */
function mystatistics_add_record_statistics($nid, $uid) {
  /*$query = db_insert('mystatistics');
  $query->fields(array(
    'nid' => $nid,
    'uid' => $uid,
    'time' => date('Y-m-d G:i:s'),
  ));
  $query->execute();*/
  
  // Create instance of entity
  $my_entity = entity_create('mystatistics', array(
    'nid' => $nid,
    'uid' => $uid,
    'time' => date('Y-m-d G:i:s'),
  ));
  $my_wrapper = entity_metadata_wrapper('mystatistics', $my_entity);
  $my_wrapper->save();
  
  
}

/**
 * Clears statistics table in database
 */
//function mystatistics_clear($form, &$form_state) {
function mystatistics_clear() {
  $query = db_delete('mystatistics');
  $query->execute();
  drupal_set_message(t('Statistics cleared'), 'status');
}

/**
 * Clears statistics table in database
 * from records about particular node
 */
//function mystatistics_clear($form, &$form_state) {
function mystatistics_node_clear($nid) {
  $query = db_delete('mystatistics');
  $query->condition('nid', $nid, '=');
  $query->execute();
  drupal_set_message(t('Statistics cleared'), 'status');
}

/**
 * Gets statistics of views for one user and one node
 * Funciton may be useful to get small amounts of statistics
 * @param $nid - node id to count views for
 * @param $uid - user id to count views for
 * 
 * @return int - number of times the user viewed the node
 */
function mystatistics_get_statistics_particular($nid, $uid) {
  $query = db_select('mystatistics', 'm');
  $query->addExpression('COUNT(*)', 'views');
  $query->condition('nid', $nid, '=');
  $query->condition('m.uid', $uid, '=');
  $results = $query->execute();
  return $results->views; 
}

/**
 * Draws a table with statistics on the page
 * @param type $node
 * @return type
 */
function mystatistics_show_statistics($node) {
  //dsm($node);
  $header = array(
    array('data' => 'User', 'field' => 'name', 'sort' => 'ASC'),
    array('data' => 'Views', 'field' => 'views', 'sort' => 'ASC'),
    );
  $query = db_select('mystatistics', 'm')
    ->extend('PagerDefault')->limit(10)
    ->extend('TableSort')->orderByHeader($header);
  $query->join('users', 'u', 'm.uid = u.uid');
  $query->fields('u', array('name','uid'));
  $query->addExpression('COUNT(*)', 'views');
  $query->condition('nid', $node->nid, '=');
  //$query->condition('m.uid', 0, '>');
  $query->groupBy('name');
  $results = $query->execute();
  $rows = array();
  foreach ($results as $row){
    //$rows[] = array();
    if ($row->uid != 0) {
      $rows[] = array( 
        l($row->name, '/user/' . $row->uid),
        //'<a href="/user/' . $row->uid . '">' . $row->name . '</a>',
        $row->views);
    } 
    else {
      $rows[] = array('Anonimous', $row->views);
    }
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('pager');
  $output .= theme('sort');
  //dpm($output);
  return $output;
}

/**
 * Draws statistics as a form with table
 * 
 * @param type $form
 * @param type $form_state
 * @param type $node
 * @return type
 */
function mystatistics_statistics_table_form($form, &$form_state, $node) {
  //dpm($form_state);
  $are_vars_set = isset(
    $form_state['mystatistics'],
    $form_state['mystatistics']['ask_confirm']
  );
  
  // If confirmation to clear is needed draw a confirmation form
  if ($are_vars_set && $form_state['mystatistics']['ask_confirm']) {
    $form_state['mystatistics'] = array('nid' => $node->nid);
    $question = t('Clear statistics for this node?');
    $path = 'node/%node/statistics_form';
    return confirm_form($form, $question, $path);
  }
  // else draw statistics form
  else {
    $form['mystatistics_clear'] = array(
      '#type' => 'submit',
      '#value' => 'Clear statistics for node',
      '#weight' => 2,
    );
    $form['mystatistics_table'] = array(
      '#title' => 'Statistics',
      '#markup' => t(mystatistics_show_statistics($node)),
    );
    return $form;
  }
}

function mystatistics_statistics_table_form_submit($form, &$form_state) {
  if (isset($form_state['values']['confirm']) && $form_state['values']['confirm']) {
    $nid = $form_state['mystatistics']['nid'];
    mystatistics_node_clear($nid);
    $msg = t('Statistics for node cleared');
    drupal_set_message($msg);
  }
  else {
    $form_state['rebuild'] = TRUE;
    $form_state['mystatistics'] = array(
      'ask_confirm' => TRUE,
    );
  }
}
