<?php

/**
 * Draws module settings form
 * 
 * @param type $form
 * @param type $form_state
 * @return type
 */
function mystatistics_settings_form($form, &$form_state) {
  // Get default values for roles
  $mystatistics_roles = variable_get('mystatistics_roles');
  // Get roles from db 
  $roles = array();
  $query = db_select('role','r');
  $query->fields('r',array('rid', 'name'));
  $result = $query->execute();
  foreach($result as $res){
    $roles[$res->rid] = $res->name;    
  }
  $form['mystatistics_roles'] = array(
    '#title' => t('Roles to watch'),
    '#type' => 'checkboxes',
    '#options' => $roles,
    '#default_value' => $mystatistics_roles,
  );

// Get default values for roles
  $mystatistics_node_types = variable_get('mystatistics_node_types');
  $node_types = array();
  $query = db_select('node_type','n');
  $query->fields('n',array('type', 'name'));
  $result = $query->execute();
  foreach($result as $res){
    $node_types[$res->type] = $res->name;
  }
  //dpm($node_types);
  //dpm($mystatistics_node_types);
  // Add node types to form as checkboxes
  $form['mystatistics_node_types'] = array(
    '#title' => t('Node types to watch'),
    '#type' => 'checkboxes',
    '#options' => $node_types,
    '#default_value' => $mystatistics_node_types,
  );
  return system_settings_form($form);
}  

/**
 * Shows confirmation form for clearing all statistics
 * 
 */
function mystatistics_confirm_clear_form($form, &$form_state) {
  return confirm_form($form, t('Clear all statistics?'), 'admin/config/mystatistics');
}

/**
 * Clears all statistic on action confirmation
 *
 */
function mystatistics_confirm_clear_form_submit($form, &$form_state) {
  mystatistics_clear();
  drupal_set_message(t('Statisctics is cleared'), 'status');
}

/**
 * Shows confirmatioon form for clearing statistics for a node
 *
 */
function mystatistics_confirm_node_clear_form($form, &$form_state, $node) {
  $form_state['mystatistics'] = array(
    'nid' => $node->nid,
    'node_title' => $node->title,
  );
  $question = t('Clear statistics for this node "' . $node->title . '"?');
  $path = 'node/' . $node->nid . '/statistics';
  return confirm_form($form, $question, $path);
}

/**
 * Clears statistics for a node on action confirmation. Then redirects to
 * statistics page
 * 
 */
function mystatistics_confirm_node_clear_form_submit($form, &$form_state) {
  // Function called on submit of confirm_node_clear_form. Url for form page is
  // node/%node/statistics/clear. Node id should be availiable from url
  $nid = $form_state['mystatistics']['nid'];
  mystatistics_node_clear($nid);
  drupal_set_message(t('Statistics is cleared for node "%node_title"',
      array('%node_title' => $form_state['mystatistics']['node_title'])),
      'status');
  $form_state['redirect'] = 'node/' . $nid . '/mystatistics';
}
